package com.example.demo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
class SomadorControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void test10And9Gives19() throws Exception {
		RequestBuilder requestBuilder = get("/sum")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.param("a", "10")
				.param("b", "9");

		ResultActions actions = mockMvc.perform(requestBuilder);

		actions.andExpect(status().isOk())
		.andExpect(content().contentType("application/json"))
		.andExpect(jsonPath("$.a").value(10))
		.andExpect(jsonPath("$.b").value(9))
		.andExpect(jsonPath("$.x").value(19));
	}

	@Test
	void testMinus10And4GivesMinus6() throws Exception {
		RequestBuilder requestBuilder = get("/sum")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.param("a", "-10")
				.param("b", "4");

		ResultActions actions = mockMvc.perform(requestBuilder);

		actions.andExpect(status().isOk())
		.andExpect(content().contentType("application/json"))
		.andExpect(jsonPath("$.a").value(-10))
		.andExpect(jsonPath("$.b").value(4))
		.andExpect(jsonPath("$.x").value(-6));
	}	

	@Test
	void test15AndMinus7Gives8() throws Exception {
		RequestBuilder requestBuilder = get("/sum")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.param("a", "15")
				.param("b", "-7");

		ResultActions actions = mockMvc.perform(requestBuilder);

		actions.andExpect(status().isOk())
		.andExpect(content().contentType("application/json"))
		.andExpect(jsonPath("$.a").value(15))
		.andExpect(jsonPath("$.b").value(-7))
		.andExpect(jsonPath("$.x").value(8));
	}	
}
